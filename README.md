Appriz Cloud Services (ACS)
===================================
Disclaimer:
The use of this API is forbidden for people without permission.

===================================

###About ACS:
Appriz Cloud services is a conglomerate of services designed to give rich communication between any kind of entity (especially financial intitutions) with its customers, allowing the following features:
  
1.	Custom Rules: This feature is named “My Alerts”, and it provides the ability for end-users to configure events based on the transactional history that they want to be alerted. 
  
2.	Through  the “SmartAds Module”, the entity can send notifications that are not configurable to the customer for specific events, for example:
	
```
  a.	Your debit card was blocked because it exceeded the limit of $1000 withdrawal at ATMs in one day. 
  
```

3.		Through the “SmartAds Module” the entity can send Ads based on the transactional behavior of its customers, for example:
```
  a.	The entity can send promotional  discounts if the customer buys 2 times at “TicoBurger”.
  
  b.	Activate  a promotional banner if the customer spends at least $40 in one month at Apple Inc.
```

4.	Through the “FraudWarnings Module” the entity can alert customers if the fraud prevention system of the entity  notifies an alert. In this way, the entity can reduce the false positive. 

5.	Interchange Service Point : Appriz can act as a bridge giving the opportunity to customers to start services, for example:
 ```
  a.	Block my debit card.

  b.	Send a monthly report. 
```

6.	Appriz allows the entity to send mass messages or notifications, for example:
```
 a.	Dear Customer, our office will be closed this Christmas. Happy Holidays! 
```

<b>SmartAds is a module only available for entities that use the API-Mobile .</b>

To connect the entity with ACS, Appriz provides several methods that can be consumed with any "REST clients".  All methods are exposed as POST, so it doesn’t matter if the operation is a GET, PUT or DELETE. This due to a security issue following recommendations of OWASP about not exposing sensible information at the URI.

These methods are divided into two categories, the first category consists of all the methods that are used to interact with the mobile app, the electronic banking service, or any other service that the entity wants to give its customers (ACS-Mobile).  The second category refers to methods that are used to connect Appriz with the entity core to manipulate certain features (ACS-Backservices).


##Categories

1. [ACS-Mobil](mobile-api.md)
2. [ACS-Backservices](backservices-api.md)

