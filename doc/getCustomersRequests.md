getCustomersRequests Mehtod
==============================

This method gets all the requests made by the customers since the last call.  

##Request Data
 property  | type | required | comments
 ----------|------|----------|---------
sessionID|string|yes|This unique key is provided for each entity by the Appriz Team.
entityName|string|yes|The entity name that is registered at ACS.
 
##Response Data
 
 property | type | comments
----------|------|---------
requests|arrayOfCostumerRequest| [see details below]()
status|integer|200: OK<br>400: Bad request<br>  452: Request data missed <br> 506: Internal Error due invalid request data
error|object| use a JSON.stringify. ONLY FOR STATUS 452.

####CostumerRequest Object

property | type | comments
----------|------|---------
idMessage|string|If the request was made through a message, this field will appear with the unique ID of the message.
date|timestamp|Date of the request. 
productName|string|If the request was made through a product, this field will appear with the name of the product.
customerCode|string|This always will be returned with the Identification of the customer who did the request.
serviceCode|string|This is the code of the service given by the entity.
messageCode|string|This is the code of the advertising campaign.


##Examples
####Request
```json
{
    "sessionID": "53E9DD3F-3891-476B-B0BD-039E4CE87C79",
    "entityName": "Smartsoft Bank"
}
```



####Response 
```json
{
    "requests": [
       {
          "idMessage" : "absjdks234nsdkasdasd34u",
          "date" : 1402564612462,
          "customerCode": "1-12343-2345-6",
          "serviceCode": "BLOCK-CARD",
       },
        {
          "idMessage" : "abs543DAdskasfsddas567u",
          "date" : 1402564612465,
          "customerCode": "1-14445-3689-1",
          "serviceCode": "ENDORSE",
       },
        {
          "date" : 1402564612467,
          "customerCode": "1-22333-2375-8",
          "serviceCode": "PAY-IT",
          "productName" : "DEBIT 7725"
       },
         {
          "idMessage" : "abs543DAdskasfsddas567u",
          "date" : 1402564612468,
          "customerCode": "3-12343-2345-6",
          "messageCode": "New credit card promotion",
          "serviceCode": "SEND-INFO",
       },
    ],
    "status": 200
}
```

