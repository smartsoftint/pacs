setCustomersResponses Method
=================================

This method sends a response to a customer’s request; it receives an array, so  multiple responses could be sent through the method call.


##Request Data
 property  | type | required | comments
 ----------|------|----------|---------
sessionID|string|yes|This unique key is provided for each entity by the Appriz Team.
entityName|string|yes|The entity name that is registered at ACS.
customersResponses|arrayOfCostumerResponse|  see details below.

####CostumerResponse Object
property  | type | required | comments
----------|------|----------|---------
customerCode|string|yes|Get the customer code  from getCustomersResquests.
idMessage|string|yes|Get the Id Message  from getCustomersResquests.
productName|string||Get the product name from getCustomersResquests.
responseDate|timestamp|yes| Datetime of the response.
responseHeader|string|yes| Short message of the response.
responseDetail|string|yes| Detailed message of the response.

 
##Response Data

property | type | comments
----------|------|---------
status|integer|200: OK<br>400: Bad request<br>  452: Request data missed <br> 506: Internal Error due invalid request data
error|object| use a JSON.stringify. ONLY FOR STATUS 452.

##Examples

####Request
```json
{
    "sessionID": "53E9DD3F-3891-476B-B0BD-039E4CE87C79",
    "entityName": "Smartsoft Bank",
    "customersResponses":[
        {
          "idMessage" : "absjdks234nsdkasdasd34u",
          "responseDate" : 1402564612462,
          "customerCode": "1-12343-2345-6",
          "productName" : "MasterCard 8897",
          "responseHeader" : "Your card was blocked",
          "responseDetail" : "For your security your card was blocked. Thank you for informing us about this transaction."
        },
        {
          "idMessage" : "jkhfsdjhfw8438hkjdsjkf",
          "responseDate" : 1402564612467,
          "customerCode": "1-22333-2375-8",
          "productName" : "DEBIT 7725",
          "responseHeader" : "Thanks for your payment",
          "responseDetail" : "The payment of your telephone service was made. Thank you for using our services. "
        }
    ]
}
```


####Response 
```json
{
  "status": 200
}

