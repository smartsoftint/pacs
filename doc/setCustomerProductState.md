setCustomerProductState Mehtod
=============================================
This method enables or disables a specific product of a customer.

##Request Data
 property  | type | required | comments
 ----------|------|----------|---------
sessionID|string|yes|This unique key is provided for each entity by the Appriz Team.
entityName|string|yes|The entity name that is registered at ACS.
idCustomer|string|yes|Customer Identification.
productName|string|yes|Product name that must change the state.
state|boolean|yes| true: Enabled product<br>false: Disabled product

##Response Data
 property | type | comments
----------|------|---------
status|integer|200: OK<br>400: Bad request<br>  452: Request data missed <br> 506: Internal Error due invalid request data
error|object| use a JSON.stringify. ONLY FOR STATUS 452.

##Examples
####Request
```json
{
    "sessionID": "53E9DD3F-3891-476B-B0BD-039E4CE87C79",
    "entityName": "Smartsoft Bank",
    "idCustomer": "1-12343-2345-6",
    "productName": "Visa 9874",
    "state": true
}
```

####Response 
```json
{
    "status": 200
}
```
