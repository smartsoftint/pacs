ACS Back Services
=======================================

ACS Back Services are consumed by the banking core to interact with specific functions such as: read and respond to customer service requests; enable or disable a product or a customer. 

##Usage


REST API: This API is consumed using POST protocols, so you can use any REST client. Why POST? Simple, because the target of Appriz is to expose financial information through the web. Appriz must keep all the information encrypted and take advantage of the SSL channels, avoiding information leakeage such as client's IDs into the URL.


####C# 

```cs
private void GetPOSTResponse(Uri uri, string data, Action<Response> callback)
{
    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);

    request.Method = "POST";
    request.ContentType = "text/plain;charset=utf-8";

    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
    byte[] bytes = encoding.GetBytes(data);

    request.ContentLength = bytes.Length;

    using (Stream requestStream = request.GetRequestStream())
    {
        // Send the data.
        requestStream.Write(bytes, 0, bytes.Length);
    }

    request.BeginGetResponse((x) =>
    {
        using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(x))
        {
            if (callback != null)
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Response));
                callback(ser.ReadObject(response.GetResponseStream()) as Response);
            }
        }
    }, null);
}

```
full example: http://msdn.microsoft.com/en-us/library/jj819168.aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-2

####VB
```vb
Private Sub GetPOSTResponse(uri As Uri, data As String, callback As Action(Of Response))
    Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create(uri), HttpWebRequest)

    request.Method = "POST"
    request.ContentType = "text/plain;charset=utf-8"

    Dim encoding As New System.Text.UTF8Encoding()
    Dim bytes As Byte() = encoding.GetBytes(data)

    request.ContentLength = bytes.Length

    Using requestStream As Stream = request.GetRequestStream()
        ' Send the data.
        requestStream.Write(bytes, 0, bytes.Length)
    End Using

    request.BeginGetResponse(
        Function(x)
            Using response As HttpWebResponse = DirectCast(request.EndGetResponse(x), HttpWebResponse)
                If callback IsNot Nothing Then
                    Dim ser As New DataContractJsonSerializer(GetType(Response))
                    callback(TryCast(ser.ReadObject(response.GetResponseStream()), Response))
                End If
            End Using
            Return 0
        End Function, Nothing)
End Sub
```



full example:http://msdn.microsoft.com/en-us/library/jj819168.aspx?cs-save-lang=1&cs-lang=vb#code-snippet-2

####JAVA 
[READ THIS](http://www.vogella.com/tutorials/REST/article.html)


###Methods Availble:
 1. [getCustomersRequests](doc/getCustomersRequests.md)
 2. [setCustomersResponses](doc/setCustumersResponses.md)
 3. [setCustomerSubscription](doc/setCustomerSubscription.md)
 4. [setCustomerState](doc/setCustomerState.md)
 5. [setCustomerProductState](doc/setCustomerProductState.md)



